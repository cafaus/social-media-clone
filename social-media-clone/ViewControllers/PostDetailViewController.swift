//
//  File.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 09/03/21.
//

import Foundation
import AsyncDisplayKit
class PostDetailViewController: ASDKViewController<ASDisplayNode> {
    private let post : Post
    
    let postDetailNode : PostDetailNode
    
    init(post : Post) {
        self.post = post
        self.postDetailNode = PostDetailNode(post: self.post)
        super.init(node: ASDisplayNode())
        setupNodes()
    }
    
    
    private func setupNodes(){
        self.node.automaticallyManagesSubnodes = true
        self.node.backgroundColor = .white
        self.node.layoutSpecBlock = { _,_ in
            return ASInsetLayoutSpec(
                insets: UIEdgeInsets(
                    top: 15,
                    left: 15,
                    bottom: 0,
                    right: 15
                ),
                child:  self.postDetailNode
            )
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
