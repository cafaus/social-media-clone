//
//  AddPostViewController.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 10/03/21.
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa

class AddPostViewController: ASDKViewController<ASDisplayNode> {
    private let postListViewModel : PostListViewModel
    private let addPostNode : AddPostNode
    
    private let disposeBag = DisposeBag()
    
    var onTapAddPost : (() -> Void)?
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addPostNode.userNameField.becomeFirstResponder()
    }
    
    init(postListViewModel : PostListViewModel) {
        self.postListViewModel = postListViewModel
        self.postListViewModel.randomizeNumberForPhoto()
        self.addPostNode = AddPostNode(postListViewModel: postListViewModel)
        
        super.init(node: ASDisplayNode())
        
        setupNodes()
        bindingTextField()
        bindingBtn()
    }
    
    private func setupNodes(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Post", style: .plain, target: self, action: #selector(addPostOnClick))
        self.node.backgroundColor = .white
        self.node.automaticallyManagesSubnodes = true
        self.node.layoutSpecBlock = { _,_ in
            return ASInsetLayoutSpec(
                insets: UIEdgeInsets(
                    top: 15,
                    left: 15,
                    bottom: 0,
                    right: 15
                ),
                child: self.addPostNode
            )
        }
    }
    
    private func bindingTextField(){
        let usernameBind = self.addPostNode.userNameField.textView.rx.text
        usernameBind.orEmpty.throttle(.milliseconds(200), scheduler: MainScheduler.instance
        ).bind(to: self.postListViewModel.name)
        .disposed(by: self.disposeBag)
        
        let descriptionBind = self.addPostNode.descField.textView.rx.text
        descriptionBind.orEmpty.throttle(.milliseconds(200), scheduler: MainScheduler.instance
        ).bind(to: self.postListViewModel.description)
        .disposed(by: self.disposeBag)
    }
    
    public func bindingBtn(){
        self.postListViewModel
            .isUsernameAndDescFill
            .bind(to: (self.navigationItem.rightBarButtonItem?.rx.isEnabled)!)
            .disposed(by: self.disposeBag)
    }
    
    @objc private func addPostOnClick(){
        self.postListViewModel.addPost(imageName: self.postListViewModel.getRandomPhotosString())
        self.onTapAddPost?()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
