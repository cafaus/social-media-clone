//
//  ViewController.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 04/03/21.
//

import UIKit
import AsyncDisplayKit
import RxSwift
import RxCocoa

class HomeViewController:  ASDKViewController<ASDisplayNode>{
    
    private var homeNode : HomeNode
    private let postListViewModel : PostListViewModel
    
    var onTapShowAddPost : (() -> Void)?
    var onTapShowDetailPost : ((_ post : Post) -> Void)?
    
    private let disposeBag = DisposeBag()
    init(postListViewModel : PostListViewModel) {
        self.postListViewModel = postListViewModel
        self.homeNode = HomeNode(postListViewModel: postListViewModel)
        
        super.init(node: ASDisplayNode())
        
        self.homeNode.onTapShowDetailPost = self.showPostDetail
        setupNodes()
        
    }
    
    func setupNodes(){
        let addBarBtn = UIBarButtonItem(
            image: UIImage(systemName: "paperplane"),
            style: .plain,
            target: self,
            action: #selector(showAddPostView)
        )
        
        self.navigationItem.rightBarButtonItem = addBarBtn
        self.node.backgroundColor = .white
        self.node.automaticallyManagesSubnodes = true
        self.node.layoutSpecBlock = {_,_ in
            let stack = ASStackLayoutSpec(
                direction: .vertical,
                spacing: 0,
                justifyContent: .start,
                alignItems: .stretch,
                children: [self.homeNode]
            )
            return stack
        }
    }
    
    @objc func showAddPostView() {
        self.onTapShowAddPost?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init (coder:) has not been implemented")
    }
    
    private func showPostDetail(post : Post){
        self.onTapShowDetailPost?(post)
    }
}

