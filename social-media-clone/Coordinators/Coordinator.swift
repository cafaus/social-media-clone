//
//  Coordinator.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 08/03/21.
//

import Foundation
import UIKit

protocol  Coordinator {
    var navigationController: UINavigationController {get set}
    func start()
}
