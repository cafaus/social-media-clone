//
//  MainCoordinator.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 08/03/21.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var navigationController: UINavigationController
    let postListViewModel : PostListViewModel
    
    init(navigationController : UINavigationController) {
        self.navigationController = navigationController
        self.postListViewModel = PostListViewModel()
    }
    
    func start() {
        let vc = HomeViewController(postListViewModel : self.postListViewModel)
        vc.title = "Social Media Clone"
        vc.onTapShowAddPost = self.showAddPost
        vc.onTapShowDetailPost = self.showDetailPost
        navigationController.pushViewController(vc, animated: false)
    }
    
    private func showDetailPost(post : Post){
        let vc = PostDetailViewController(post: post)
        vc.title = "\(post.username)'s Post"
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func showAddPost(){
        let vc = AddPostViewController(postListViewModel: self.postListViewModel)
        vc.onTapAddPost =  self.goBackToRootViewController
        vc.title = "Add Post"
        self.navigationController.pushViewController(vc, animated: true)
    }

    private func goBackToRootViewController(){
        self.navigationController.popToRootViewController(animated: true)
    }
}
