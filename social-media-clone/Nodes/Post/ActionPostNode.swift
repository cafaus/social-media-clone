//
//  ActionPostNode.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 07/03/21.
//

import Foundation
import AsyncDisplayKit
import RxSwift


class ActionPostNode: ASDisplayNode {
    let loveBtn : ASButtonNode
    let commentBtn : ASButtonNode
    let shareBtn : ASButtonNode
    
    private let likeCount : BehaviorSubject<Int>

    private let loveImg : UIImage! = UIImage(named: "love")
    private let commentImg : UIImage! = UIImage(named: "comment")
    private let shareImg : UIImage! = UIImage(named: "share")
    
    init(likeCount : BehaviorSubject<Int>) {
        self.likeCount = likeCount
        
        loveBtn = ASButtonNode()
        commentBtn = ASButtonNode()
        shareBtn = ASButtonNode()
        
        super.init()
        setupNodes()
    }
    
    private func setupNodes(){
        loveBtn.style.preferredSize = CGSize(width: 30, height: 30)
        loveBtn.setImage(self.resizeImage(image: self.loveImg, newWidth: 16), for: .normal)
        loveBtn.addTarget(self, action: #selector(self.handleLikeOnClick), forControlEvents: .touchUpInside)
        
        commentBtn.style.preferredSize = CGSize(width: 30, height: 30)
        commentBtn.setImage(self.resizeImage(image: self.commentImg, newWidth: 16), for: .normal)
        
        shareBtn.style.preferredSize = CGSize(width: 30, height: 30)
        shareBtn.setImage(self.resizeImage(image: self.shareImg, newWidth: 16), for: .normal)
       
        self.automaticallyManagesSubnodes = true
    }
    
    
    private func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return image}
        UIGraphicsEndImageContext()

        return newImage
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let mainStack = ASStackLayoutSpec(
            direction: .horizontal,
            spacing: 100,
            justifyContent: .start ,
            alignItems: .stretch,
            children: [
                self.loveBtn,
                self.commentBtn,
                self.shareBtn
            ]
        )
        
        return ASCenterLayoutSpec(
            centeringOptions: .X,
            sizingOptions: [],
            child: mainStack
        )
    }
    
    @objc func handleLikeOnClick() {
        var currLikeCount = 0
        do {
            currLikeCount  = try self.likeCount.value()
        } catch  {
            print("get current like count error")
        }
        self.likeCount.onNext(currLikeCount + 1)
    }
}
