
//
//  PostDetailHeaderNode.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 09/03/21.
//

import Foundation
import AsyncDisplayKit

class PostDetailHeaderNode: ASDisplayNode {
    private let post : Post
    
    let profilePicture : ASImageNode
    let profileUsername : ASTextNode
    
    init(post : Post) {
        self.post = post
        
        profilePicture = ASImageNode()
        profilePicture.image = post.photo
        profilePicture.style.preferredSize = CGSize(width: 70, height: 70)
        profilePicture.cornerRadius = 70/2
        
        profileUsername = ASTextNode()
        let attribute = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)]
        profileUsername.attributedText = NSAttributedString(string: post.username, attributes: attribute)
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let headerStack = ASStackLayoutSpec(
            direction: .vertical,
            spacing: 10,
            justifyContent: .center,
            alignItems: .center,
            children: [profilePicture, profileUsername]
        )
        return ASCenterLayoutSpec(
            centeringOptions: .X,
            sizingOptions: [],
            child: headerStack
        )
    }
}
