//
//  PostCell.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 07/03/21.
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa

class PostCellNode : ASCellNode{
     let post : Post
     let likeNode : LikeTextNode
     let descNode : ASTextNode
     let headerPostNode : HeaderPostNode
     let actionPostNode : ActionPostNode
    
    private let disposeBag = DisposeBag()
    
    init(post : Post) {
        self.post = post
        self.likeNode = LikeTextNode(likeCount: self.post.likeCount)
        self.descNode = ASTextNode()
        self.headerPostNode = HeaderPostNode(post: self.post)
        self.actionPostNode = ActionPostNode(likeCount: self.post.likeCount)
        super.init()
        
        self.setupNodes()
        
        self.automaticallyManagesSubnodes = true
    }
    
    private func setupNodes(){
        self.descNode.attributedText = NSAttributedString(string: self.post.postDesc)
        self.descNode.maximumNumberOfLines = 0
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let descPadding = ASInsetLayoutSpec(
            insets: UIEdgeInsets(
                top: 0,
                left: 0,
                bottom: 10,
                right: 0
            ), child: self.descNode
        )
        
        let mainStack = ASStackLayoutSpec(
            direction: .vertical,
            spacing: 10,
            justifyContent: .start,
            alignItems: .start,
            children: [self.headerPostNode, descPadding, self.likeNode, self.actionPostNode]
        )
        
        return ASInsetLayoutSpec(
            insets: UIEdgeInsets(
                top: 10,
                left: 10,
                bottom: 10,
                right: 10
            ),
            child: mainStack
        )
    }
}
