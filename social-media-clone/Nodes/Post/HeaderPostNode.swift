//
//  HeaderPostNode.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 07/03/21.
//

import Foundation
import AsyncDisplayKit

class HeaderPostNode :  ASDisplayNode{
    // MARK: - Values
    let post : Post
    
    // MARK: - Nodes
    let profilePicture : ASImageNode
    let profileUsername : ASTextNode
    
    init(post : Post) {
        self.post = post
        
        profilePicture = ASImageNode()
        profilePicture.image = post.photo
        profilePicture.style.preferredSize = CGSize(width: 32, height: 32)
        profilePicture.cornerRadius = 32/2
        
        profileUsername = ASTextNode()
        let attribute = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12)]
        profileUsername.attributedText = NSAttributedString(string: post.username, attributes: attribute)
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let horizontalStack = ASStackLayoutSpec(
            direction: .horizontal,
            spacing: 15,
            justifyContent: .start,
            alignItems: .center,
            children: [profilePicture, profileUsername]
        )
        
        return ASInsetLayoutSpec(
            insets: UIEdgeInsets(
                top: 8,
                left: 8,
                bottom: 4,
                right: 8
            ),
            child: horizontalStack
        )
    }
}
