//
//  AddPostNode.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 23/03/21.
//

import Foundation
import AsyncDisplayKit

class AddPostNode: ASDisplayNode {
    let imgProfile : ASImageNode
    let userNameField : ASEditableTextNode
    let descField : ASEditableTextNode
    let postListViewModel : PostListViewModel
    
    init(postListViewModel : PostListViewModel) {
        self.imgProfile = ASImageNode()
        self.userNameField = ASEditableTextNode()
        self.descField = ASEditableTextNode()
        self.postListViewModel = postListViewModel
        
        super.init()
        self.setupNodes()
    }
    
    private func setupNodes(){
        self.imgProfile.image = UIImage(named: self.postListViewModel.getRandomPhotosString())
        self.imgProfile.style.preferredSize = CGSize(width: 70, height: 70)
        self.imgProfile.cornerRadius = 70/2
        
        self.userNameField.attributedPlaceholderText = NSAttributedString(string: "What is your name?", attributes: self.postListViewModel.placholderAttribute)
        self.descField.attributedPlaceholderText = NSAttributedString(string: "What do you want to talk about?")
        self.automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let headerStack = ASStackLayoutSpec(
            direction: .vertical,
            spacing: 20,
            justifyContent: .center,
            alignItems: .center,
            children: [self.imgProfile, self.userNameField]
        )
        
        let centeredHeaderStack = ASCenterLayoutSpec(
            centeringOptions: .X,
            sizingOptions: [],
            child: headerStack
        )
            
        return ASStackLayoutSpec(
            direction: .vertical,
            spacing: 30,
            justifyContent: .start,
            alignItems: .start,
            children: [centeredHeaderStack, self.descField]
        )
    }
}
