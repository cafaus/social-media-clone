//
//  PostDetailNode.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 23/03/21.
//

import Foundation
import AsyncDisplayKit

class PostDetailNode: ASDisplayNode {
    
    let postDetailHeaderNode : PostDetailHeaderNode
    let descNode : ASTextNode
    let likeNode : LikeTextNode
    let actionPostNode : ActionPostNode
    
    private let post : Post
    
    init(post : Post) {
        self.post = post
        self.postDetailHeaderNode = PostDetailHeaderNode(post: self.post)
        self.actionPostNode = ActionPostNode(likeCount: post.likeCount)
        self.likeNode = LikeTextNode(likeCount: self.post.likeCount)
        self.descNode = ASTextNode()
        
        super.init()
        self.descNode.attributedText = NSAttributedString(
            string: self.post.postDesc, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)])
        self.automaticallyManagesSubnodes = true
        
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return ASStackLayoutSpec(
            direction: .vertical,
            spacing: 20,
            justifyContent: .start,
            alignItems: .start,
            children: [self.postDetailHeaderNode,
                       self.descNode,
                       self.likeNode,
                       self.actionPostNode
            ]
        )
    }
}
