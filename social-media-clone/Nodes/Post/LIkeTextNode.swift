//
//  LIkeTextNode.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 09/03/21.
//

import Foundation
import AsyncDisplayKit
import RxSwift

class LikeTextNode: ASTextNode {
    let likeCount : BehaviorSubject<Int>
    let disposeBag = DisposeBag()
    
    private let likeAttribute = [
        NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 10),
        NSAttributedString.Key.foregroundColor : UIColor.red
    ]
    
    init(likeCount : BehaviorSubject<Int>) {
        self.likeCount = likeCount
        super.init()
        self.likeCount.subscribe(onNext: { [weak self] likeCount in
            if likeCount > 1 {
                self?.attributedText = NSAttributedString(string: "\(likeCount) Likes", attributes: self?.likeAttribute)
            } else {
                self?.attributedText = NSAttributedString(string: "\(likeCount) Like", attributes: self?.likeAttribute)
            }
            
        }).disposed(by: self.disposeBag)
    }
}
