//
//  HomeNode.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 08/03/21.
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa

class HomeNode : ASTableNode{
    private let postListViewModel : PostListViewModel
    private let disposeBag = DisposeBag()
    var onTapShowDetailPost : ((_ post : Post) -> Void)?
    
    init(postListViewModel : PostListViewModel) {
        self.postListViewModel = postListViewModel
        super.init(style: .plain)
        setup()
    }
    
    private func setup(){
        self.dataSource = self
        self.delegate = self
        
        self.style.width = ASDimension(unit: .fraction, value: 1)
        self.style.height = ASDimension(unit: .fraction, value: 1)
        self.style.flexShrink = 1
        
        self.postListViewModel.posts.subscribe(onNext: {_ in
            self.performBatchUpdates({
                self.insertRows(at: [IndexPath(item: 0, section: 0)], with: .automatic)
            }, completion: nil)
            
        }).disposed(by: self.disposeBag)
    }
}

extension HomeNode : ASTableDataSource, ASTableDelegate{
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return 1
    }

    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        
        return self.postListViewModel.posts.value.count
    }

    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        let posts = self.postListViewModel.posts.value

        guard posts.count > indexPath.row else {return {ASCellNode()}}

        let post = posts[indexPath.row]
      
        let cellNodeBlock = { () -> ASCellNode in
            return  PostCellNode(post: post)

        }

        return cellNodeBlock
    }

    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
        let posts = self.postListViewModel.posts.value
        self.onTapShowDetailPost?(posts[indexPath.row])
    }
}
