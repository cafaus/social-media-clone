//
//  PostListViewModel.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 07/03/21.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class PostListViewModel {
    let posts : BehaviorRelay<[Post]>
    let name : BehaviorRelay<String>
    let description : BehaviorRelay<String>
    let isUsernameAndDescFill : BehaviorSubject<Bool>
    
    private let photos = ["people-1","people-2","people-3","people-4"]
    
    let placholderAttribute : [NSAttributedString.Key : Any] = [
        .font : UIFont.systemFont(ofSize: 18, weight: UIFont.Weight(rawValue: 0.1)),
        .foregroundColor : UIColor.gray
    ]
    
    private var randomNum = 0
    
    private let disposeBag = DisposeBag()
    
    init(name: String = "", description: String = "", posts: [Post] = Post.dummyPost()) {
        self.posts = BehaviorRelay<[Post]>.init(value: posts)
        self.name = BehaviorRelay<String>.init(value: name)
        self.description = BehaviorRelay<String>(value: description)
        self.isUsernameAndDescFill = BehaviorSubject<Bool>(value: false)
        self.randomNum = Int.random(in: 0...3)
        self.configureIsUsernameAndDescFillObs()
    }
    
    public func addPost(imageName : String){
        let newPost = Post(username: self.name.value, postDesc: self.description.value, photo: UIImage(named: imageName)!)
        
        var posts = self.posts.value
        posts.insert(newPost, at: 0)
        
        self.posts.accept(posts)
    }
    
    public func randomizeNumberForPhoto(){
        self.randomNum = Int.random(in: 0...3)
    }
    
    public func getRandomPhotosString() -> String{
        return self.photos[self.randomNum]
    }
    
    private func configureIsUsernameAndDescFillObs(){
        Observable.combineLatest(self.name.asObservable(), self.description.asObservable()).map({ (arg0) -> Bool in
            let (username, description) = arg0
            return !username.isEmpty && !description.isEmpty
        }).bind(to: self.isUsernameAndDescFill)
        .disposed(by: disposeBag)
    }
}
