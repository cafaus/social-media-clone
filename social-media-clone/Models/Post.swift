//
//  Post.swift
//  social-media-clone
//
//  Created by Calvin Faustine on 07/03/21.
//

import Foundation
import UIKit
import RxSwift

class Post{
    let username : String
    let postDesc : String
    let photo : UIImage
    let likeCount : BehaviorSubject<Int>
    
    init(username : String, postDesc: String, photo : UIImage) {
        self.username = username
        self.postDesc = postDesc
        self.photo = photo
        self.likeCount = BehaviorSubject<Int>(value: 0)
    }
    
    public static func dummyPost() -> [Post]{
        let user1 = Post(username: "Angeline", postDesc: "its my first post!!", photo: UIImage(named: "people-1")!)
        let user2 = Post(username: "Tony", postDesc: "Say hi to me", photo: UIImage(named: "people-2")!)
        let user3 = Post(username: "Cynthia", postDesc: "its my second post", photo: UIImage(named: "people-3")!)
        let user4 = Post(username: "Michelle", postDesc: "Say hi to me guys!!", photo: UIImage(named: "people-4")!)
        return [user1, user2, user3, user4]
    }
}
