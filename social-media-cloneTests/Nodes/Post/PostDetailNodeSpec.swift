//
//  PostDetailNode.swift
//  social-media-cloneTests
//
//  Created by Calvin Faustine on 19/03/21.
//
import Foundation
import Quick
import Nimble

@testable import social_media_clone

class PostDetailNodeSpec: QuickSpec {

    
    override func spec() {
        var postDetailNode : PostDetailNode!
        beforeEach {
            guard let image = UIImage(named: "people-1") else {
                print("wrong name image")
                return
            }
            let dummyPost = Post(username: "calvin", postDesc: "hello world", photo: image)
            postDetailNode = PostDetailNode(post: dummyPost)
        }
        
        afterEach {
            postDetailNode = nil
        }
        
        describe("header node") {
            context("init header node"){
                it("username should match with calvin") {
                    expect(postDetailNode.postDetailHeaderNode.profileUsername.attributedText?.string).to(equal("calvin"))
                }
            }
        }
        
        describe("description node"){
            context("init description node"){
                it("description should match with hello world"){
                    expect(postDetailNode.descNode.attributedText?.string).to(equal("hello world"))
                }
            }
        }
        
        describe("like feature"){
            context("init like node"){
                it("sets like count to 0") {
                    expect(self.getLikeCount(postDetailNode: postDetailNode)).to(equal(0))
                }
            }
            context("like button tapped"){
                it("should add like count by one"){
                    postDetailNode.actionPostNode.loveBtn.sendActions(forControlEvents: .touchUpInside, with: nil)
                    expect(self.getLikeCount(postDetailNode: postDetailNode)).to(equal(1))
                }
            }
        }
    }
    
    private func getLikeCount(postDetailNode : PostDetailNode) -> Int{
        do {
            return try postDetailNode.likeNode.likeCount.value()
        } catch {
            return 0
        }
    }

}
