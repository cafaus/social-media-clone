//
//  PostCellNodeSpec.swift
//  social-media-cloneTests
//
//  Created by Calvin Faustine on 19/03/21.
//

import Foundation
import Quick
import Nimble

@testable import social_media_clone

class PostCellNodeSpec: QuickSpec {
    override func spec() {
        var postCellNode : PostCellNode!
        var dummyPost : Post!
        
        beforeEach {
            guard let image = UIImage(named: "people-1") else {
                print("wrong name image")
                return
            }
            dummyPost = Post(username: "calvin", postDesc: "hello world", photo: image)
            postCellNode = PostCellNode(post: dummyPost)
        }
        
        afterEach {
            postCellNode = nil
            dummyPost = nil
        }
        
        describe("PostCellNode") {
            context("init PostCellNode"){
                it("description should match with hello world") {
                    expect(postCellNode.descNode.attributedText?.string).to(equal("hello world"))
                }
                
                it("username should match with calvin") {
                    expect(postCellNode.headerPostNode.profileUsername.attributedText?.string).to(equal("calvin"))
                }
                
                
            }
        }
        
        describe("like count"){
            context("like button tapped"){
                it("sets like count to 0"){
                    expect(self.getLikeCount(postCellNode: postCellNode)).to(equal(0))
                }
                it("should add like count by one") {
                    postCellNode.actionPostNode.loveBtn.sendActions(forControlEvents: .touchUpInside, with: nil)
                    expect(self.getLikeCount(postCellNode: postCellNode)).to(equal(1))
                }
            }
        }
    }
    
    private func getLikeCount(postCellNode : PostCellNode) -> Int{
        do {
            return try postCellNode.likeNode.likeCount.value()
        } catch {
            return 0
        }
    }
}
