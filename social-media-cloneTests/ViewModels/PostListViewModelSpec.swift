//
//  PostListViewModelSpec.swift
//  social-media-cloneTests
//
//  Created by Calvin Faustine on 20/03/21.
//
import Foundation
import Quick
import Nimble
import RxBlocking

@testable import social_media_clone
class PostListViewModelSpec: QuickSpec {
    override func spec() {
        var postListViewModel : PostListViewModel!
        
        beforeEach {
            postListViewModel = PostListViewModel()
        }
        afterEach {
            postListViewModel = nil
        }
        
        describe("Post List") {
            beforeEach {
                
            }
            
            afterEach {
                postListViewModel = nil
            }
            context("init post"){
                it("posts count should equal 4") {
                    let postCount = postListViewModel.posts.value.count
                    expect(postCount).to(equal(4))
                }
            }
            
            context("add post"){
                it("posts count should equal 5") {
                    postListViewModel.addPost(imageName: "people-1")
                    let postCount = postListViewModel.posts.value.count
                    expect(postCount).to(equal(5))
                }
                
            }
        }
        
        describe("is username and desc fill") {
            context("name and description is filled") {
                it("isUsernameAndDescFill variable should true") {
                    postListViewModel = PostListViewModel(name: "calvin", description: "hello world")
                    expect(self.getIsUsernameAndDescFillValue(postListViewModel: postListViewModel)).to(beTrue())
                }
            }
            
            context("name and description not filled"){
                it("isUsernameAndDescFill variable should false") {
                    expect(self.getIsUsernameAndDescFillValue(postListViewModel: postListViewModel)).to(beFalse())
                }
            }
            
            context("name is not filled") {
                it("isUsernameAndDescFill variable should false") {
                    postListViewModel = PostListViewModel( description: "hello world")
                    expect(self.getIsUsernameAndDescFillValue(postListViewModel: postListViewModel)).to(beFalse())
                }
            }
            
            context("description is not filled") {
                it("isUsernameAndDescFill variable should false") {
                    postListViewModel.name.accept("hello world")
                    expect(self.getIsUsernameAndDescFillValue(postListViewModel: postListViewModel)).to(beFalse())
                }
            }
        }
    }
    
    
    private func getIsUsernameAndDescFillValue( postListViewModel: PostListViewModel) -> Bool{
        do {
            return try postListViewModel.isUsernameAndDescFill.toBlocking().first() ?? false
        } catch {
            return false
        }
    }
    
}
